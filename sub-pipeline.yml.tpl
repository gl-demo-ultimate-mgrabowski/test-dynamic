trigger_123:
  trigger:
    include: .gitlab-ci.yml
    strategy: depend
  variables:
    GITLAB_CHART_VERSION: 123
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH